module federai/robot

go 1.15

require (
	github.com/gofrs/uuid v3.3.0+incompatible // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/veandco/go-sdl2 v0.4.4 // indirect
	gobot.io/x/gobot v1.14.0
	golang.org/x/net v0.0.0-20201031054903-ff519b6c9102 // indirect
)
