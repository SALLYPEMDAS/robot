package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"

	"gobot.io/x/gobot"
	"gobot.io/x/gobot/platforms/firmata"
	"gobot.io/x/gobot/platforms/mqtt"
)

//Motor3(7, 6);
//Motor4(4, 5);

type Motor struct {
	DirPin   string
	SpeedPin string
	Adaptor  *firmata.Adaptor
}

func (m *Motor) Set(speed int) {
	dir := 1
	if speed < 0 {
		speed = 0 - speed
		dir = 0
	}

	if speed > 254 {
		speed = 254
	}

	m.Adaptor.DigitalWrite(m.DirPin, byte(dir))
	m.Adaptor.PwmWrite(m.SpeedPin, byte(speed))
}

func main() {
	firmataAdaptor := firmata.NewAdaptor(findArduino())
	mqttAdaptor := mqtt.NewAdaptor("tcp://10.0.0.226:1883", "edge")
	motorLeft := &Motor{
		DirPin:   "7",
		SpeedPin: "6",
		Adaptor:  firmataAdaptor,
	}

	motorRight := &Motor{
		DirPin:   "4",
		SpeedPin: "5",
		Adaptor:  firmataAdaptor,
	}

	work := func() {
fmt.Println("SetMotorLeft")

motorLeft.Set(30)
fmt.Println("SetMotorRight")
motorRight.Set(30)



		mqttAdaptor.On("rightStick", func(msg mqtt.Message) {
			fmt.Println(string(msg.Payload()))

			i, _ := strconv.Atoi(string(msg.Payload()))
			motorRight.Set(i)
		})
		mqttAdaptor.On("leftStick", func(msg mqtt.Message) {
			fmt.Println(string(msg.Payload()))

			i, _ := strconv.Atoi(string(msg.Payload()))
			motorLeft.Set(i)
		})

	}

	robot := gobot.NewRobot("bot",
		[]gobot.Connection{firmataAdaptor},
		[]gobot.Connection{mqttAdaptor},

		work,
	)

	robot.Start()
}

func findArduino() string {
	contents, _ := ioutil.ReadDir("/dev")

	// Look for what is mostly likely the Arduino device
	for _, f := range contents {
		if strings.Contains(f.Name(), "tty.usbserial") ||
			strings.Contains(f.Name(), "ttyUSB") {
			return "/dev/" + f.Name()
		}
	}

	// Have not been able to find a USB device that 'looks'
	// like an Arduino.
	return ""
}
