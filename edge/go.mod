module federai/robot/edge

go 1.15

require (
	github.com/creack/goselect v0.1.1 // indirect
	github.com/go-ble/ble v0.0.0-20200407180624-067514cd6e24 // indirect
	github.com/gofrs/uuid v3.3.0+incompatible // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.0 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/raff/goble v0.0.0-20200327175727-d63360dcfd80 // indirect
	go.bug.st/serial.v1 v0.0.0-20191202182710-24a6610f0541 // indirect
	gobot.io/x/gobot v1.14.0
	golang.org/x/net v0.0.0-20201031054903-ff519b6c9102 // indirect
	golang.org/x/sys v0.0.0-20201106081118-db71ae66460a // indirect
)
