package main

import (
	"fmt"
	"strconv"
	"sync/atomic"
	"time"

	"gobot.io/x/gobot"
	"gobot.io/x/gobot/platforms/joystick"
	"gobot.io/x/gobot/platforms/mqtt"
)

var leftY, rightY atomic.Value

func main() {
	fmt.Println("1")
	mqttAdaptor := mqtt.NewAdaptor("tcp://xanatos.local:1883", "pinger")
	joystickAdaptor := joystick.NewAdaptor()
	stick := joystick.NewDriver(joystickAdaptor, joystick.Xbox360)

	fmt.Println("2")

	work := func() {
		leftY.Store(float64(0.0))

		rightY.Store(float64(0.0))

		stick.On(joystick.RightY, func(data interface{}) {
			val := float64(data.(int16))
			fmt.Println(val)
			rightY.Store(val)
		})

		stick.On(joystick.LeftY, func(data interface{}) {
			val := float64(data.(int16))
			fmt.Println(val)
			leftY.Store(val)
		})

		gobot.Every(50*time.Millisecond, func() {
			right := rightY.Load().(float64)
			fmt.Printf("%v\n\n", right)
			switch {
			default:
				mqttAdaptor.Publish("rightStick", prepInput(right))
			}
		})

		gobot.Every(50*time.Millisecond, func() {
			left := leftY.Load().(float64)
			fmt.Printf("%v\n\n", left)
			switch {
			default:
				mqttAdaptor.Publish("leftStick", prepInput(left))
			}
		})

		mqttAdaptor.On("rightStick", func(msg mqtt.Message) {
			fmt.Println(string(msg.Payload()))
		})
		mqttAdaptor.On("leftStick", func(msg mqtt.Message) {
			fmt.Println(string(msg.Payload()))
		})

	}

	fmt.Println("3")

	robot := gobot.NewRobot("mqttBot",
		[]gobot.Connection{mqttAdaptor},
		[]gobot.Connection{joystickAdaptor},
		[]gobot.Device{stick},

		work,
	)

	fmt.Println("4")

	robot.Start()

	fmt.Println("5")
}

func prepInput(input float64) []byte {
	if input == float64(0) {
		return []byte(strconv.Itoa(0))
	}

	input = (input / 32768) * 255

	return []byte(strconv.Itoa(int(input)))
}
